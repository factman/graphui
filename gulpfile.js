/* eslint-disable no-console */
"use strict";

const { dest, src, parallel, series, watch } = require("gulp"),
  minify = require("gulp-terser"),
  rename = require("gulp-rename"),
  del = require("del"),
  sass = require("gulp-sass"),
  maps = require("gulp-sourcemaps"),
  minify_css = require("gulp-uglifycss");

sass.compiler = require("node-sass");

let destDir = "dev";
const appName = "GraphUI";
const docsDir = "docs";
const distDir = "dist";

function minifyJs() {
  return src(`${destDir}/js/${appName}.js`)
    .pipe(maps.init())
    .pipe(minify())
    .pipe(rename(appName + ".min.js"))
    .pipe(maps.write("./"))
    .pipe(dest(`${destDir}/js`));
}

function minifyCSS() {
  return src(`${destDir}/css/${appName}.css`)
    .pipe(maps.init())
    .pipe(minify_css({
      "uglyComments": true
    }))
    .pipe(rename(appName + ".min.css"))
    .pipe(maps.write("./"))
    .pipe(dest(`${destDir}/css`));
}

function compileSass() {
  if (destDir === "dist") {
    return src("./src/scss/**/*.scss")
      .pipe(sass.sync().on("error", sass.logError))
      .pipe(rename(appName + ".css"))
      .pipe(dest(`${destDir}/css`));
  } else {
    return src("./src/scss/**/*.scss")
      .pipe(sass.sync().on("error", sass.logError))
      .pipe(rename(appName + ".css"))
      .pipe(dest(`${destDir}/css`));
  }
}

function compileJs(cb) {
  return series(
    moveFiles.bind(this, "./src/app.js", `${destDir}/js`, null, `${appName}.js`),
    () => Promise.resolve(cb())
  )();
}

function moveFiles(srcFile, destFolder, base = null, fileName) {
  if (typeof fileName === "string") {
    return src(srcFile, { base })
      .pipe(rename(fileName))
      .pipe(dest(destFolder));
  } else {
    return src(srcFile, { base })
      .pipe(dest(destFolder));
  }
}

function moveDocs(cb) {
  return series(
    moveFiles.bind(this, "./docs/**/*.*", `${destDir}/docs/`, "./docs/"),
    () => Promise.resolve(cb())
  )();
}

function clean(dir) {
  return Promise.resolve(del(dir));
}

function printMessage(message) {
  return Promise.resolve(console.log(message));
}

function dev(cb) {
  return series(
    clean.bind(this, ["dev"]),
    parallel(
      compileSass,
      moveFiles.bind(this, "./src/app.js", `${destDir}/js`, null, `${appName}.js`),
      moveFiles.bind(this, "./src/js/script.js", `${destDir}/js`, null),
      moveFiles.bind(this, "./src/js/script.js", `${docsDir}/js`, null),
      moveFiles.bind(this, "./src/css/*.css", `${destDir}/css`, null),
      moveFiles.bind(this, "./src/css/style.css", `${docsDir}/css`, null)
    ),
    printMessage.bind(this, "Development Ready."),
    () => Promise.resolve(cb())
  )();
}

function build(cb) {
  destDir = "build";
  return series(
    clean.bind(this, ["build"]),
    parallel(
      compileSass,
      compileJs,
      moveFiles.bind(this, "./src/js/script.js", `${docsDir}/js`, null),
      moveFiles.bind(this, "./src/css/style.css", `${docsDir}/css`, null)
    ),
    parallel(
      minifyJs,
      minifyCSS
    ),
    moveDocs,
    printMessage.bind(this, "Build Process Completed."),
    () => Promise.resolve(cb())
  )();
}

function dist(cb) {
  return series(
    dev,
    build,
    parallel(
      moveFiles.bind(this, "./build/**/*", `${distDir}`, "./build/"),
      moveFiles.bind(this, "./CHANGELOG.md", `${distDir}`, null),
      moveFiles.bind(this, "./README.md", `${distDir}`, null),
      moveFiles.bind(this, "./LICENSE", `${distDir}`, null)
    ),
    () => Promise.resolve(cb())
  )();
}

function watchJs() {
  return Promise.resolve(
    moveFiles
      .bind(this, "./src/app.js", `${destDir}/js`, null, `${appName}.js`)()
  );
}

function watchCss() {
  return Promise.resolve(
    moveFiles
      .bind(this, "./src/css/*.css", `${destDir}/css`, null)()
  );
}

function watchScss() {
  return Promise.resolve(compileSass());
}

exports.dev = dev;

exports.build = build;

exports.dist = dist;

exports.watch = function() {
  watch("src/app.js", { ignoreInitial: false, events: "all", queue: false }, series(watchJs));
  watch("src/scss/style.scss", { ignoreInitial: false, events: "all", queue: false }, series(watchScss));
  watch("src/css/*.css", { ignoreInitial: false, events: "all", queue: false }, series(watchCss));
};

exports.default = function(cb) {
  return series(
    dev,
    printMessage.bind(this, "Gulp Tasks Completed."),
    () => Promise.resolve(cb())
  )();
};
